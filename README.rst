Goal
====

Python modules downloader for static analysis and other purposes.

Dependencies
============

* python-colorama

* pkgtools (from PyPI)

* Requests

Usage
=====

*  Run grabeggs.py

   ::

     $ python grabeggs.py

Downloaded data is in ``data/`` folder
