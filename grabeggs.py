#!/usr/bin/env python

from __future__ import print_function
from colorama import init
init()
from colorama import Fore, Back, Style
from pkgtools.pypi import PyPIXmlRpc
import requests
import os
import multiprocessing
import threading
import sys

# global stuff
lock = threading.Lock()
pypi = PyPIXmlRpc()
pkgs = pypi.list_packages()
# pkgs = ["pkgtools", "django"]
total = len(pkgs)
i = 0.0

def fetch(url, destination):
    r = requests.get(url)
    with open(destination, "wb") as f:
        f.write(r.content)

def analyze(pkg):
    pypit = PyPIXmlRpc()
    ver = pypit.package_releases(pkg)
    if not ver:
        return
    ver = ver[0]
    releases = pypit.release_urls(pkg, ver)
    if releases:
        for release in releases:
            if "url" not in release:
                continue
            url = release["url"]
            filename = release["filename"]
            if not filename.endswith("exe") and (filename.endswith(".tar.gz")
                or filename.endswith(".tar.bz2") or filename.endswith(".zip")):
                break

        destination = os.path.join("data", filename)
        if os.path.exists(destination):
            print(Fore.GREEN + "[*] We already have", filename)
        if not filename.endswith("exe"):
            print(Fore.MAGENTA + "Fetching", url)
            fetch(url, destination)


def output_callback(data):
    global i
    with lock:
        i = i + 1
        print(Fore.YELLOW + "[+] %f done ..." % ((i /  total) * 100))
        sys.stdout.flush()


p = multiprocessing.Pool(16)
for pkg in pkgs:
    p.apply_async(analyze, (pkg,), callback = output_callback)

p.close()
p.join()

